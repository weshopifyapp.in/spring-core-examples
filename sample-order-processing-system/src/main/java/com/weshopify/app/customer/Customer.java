package com.weshopify.app.customer;

public class Customer {

	private int cid;
	private String cName;
	private String email;
	
	private String mobile;
	
	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Customer() {
		System.out.println("Customer Bean is creating");
	}
	
	@Override
	public String toString() {
		return "Customer [cid=" + cid + ", cName=" + cName + ", email=" + email + ", mobile=" + mobile + "]";
	}
}
