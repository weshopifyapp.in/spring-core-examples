package com.weshopify.app.customer;

import java.util.List;
import java.util.Properties;

public class Address {

	private String pinCode;
	private String area;
	private String flatNo;
	private List<String> landMarks;
	
	private Properties addressProofs;
	
	public Properties getAddressProofs() {
		return addressProofs;
	}
	public void setAddressProofs(Properties addressProofs) {
		this.addressProofs = addressProofs;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getFlatNo() {
		return flatNo;
	}
	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}
	public List<String> getLandMarks() {
		return landMarks;
	}
	public void setLandMarks(List<String> landMarks) {
		this.landMarks = landMarks;
	}
	@Override
	public String toString() {
		return "Address [pinCode=" + pinCode + ", area=" + area + ", flatNo=" + flatNo + ", landMarks=" + landMarks
				+ ", addressProofs=" + addressProofs + "]";
	}
		
	
}
