package com.weshopify.app.order;

import java.util.Map;
import java.util.Set;

import com.weshopify.app.customer.Address;
import com.weshopify.app.customer.Customer;
import com.weshopify.app.product.Product;

public class OrderDetails {

	public OrderDetails() {
		System.out.println("OrderDetails bean is creating");
	}
	private String orderId;
	private double netPrice;
	
	private Set<Product> productSet;
	
	private Set<String> landMarks;
	
	private Map<String,Address> addressMap;
	
	public Map<String, Address> getAddressMap() {
		return addressMap;
	}
	public void setAddressMap(Map<String, Address> addressMap) {
		this.addressMap = addressMap;
	}
	public Set<String> getLandMarks() {
		return landMarks;
	}
	public void setLandMarks(Set<String> landMarks) {
		this.landMarks = landMarks;
	}
	public Set<Product> getProductSet() {
		return productSet;
	}
	public void setProductSet(Set<Product> productSet) {
		this.productSet = productSet;
	}
	private Customer customer;
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public double getNetPrice() {
		return netPrice;
	}
	public void setNetPrice(double netPrice) {
		this.netPrice = netPrice;
	}
	
	@Override
	public String toString() {
		return "OrderDetails [orderId=" + orderId + ", netPrice=" + netPrice + ", productSet=" + productSet
				+ ", landMarks=" + landMarks + ", customer=" + customer + "]";
	}
	
	
}
