package com.weshopify.app.container;

import com.weshopify.app.customer.Customer;
import com.weshopify.app.order.OrderDetails;
import com.weshopify.app.product.Product;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class IOCContainer {

    public static void main(String[] args) {
        BeanFactory factory = new XmlBeanFactory(new ClassPathResource("order-processing.xml"));
		
		/*
		 * Customer customer = (Customer) factory.getBean("customer");
		 * System.out.println("Customer Details are:\t");
		 * System.out.println("===========================");
		 * System.out.println(customer.hashCode());
		 * System.out.println(customer.toString());
		 * 
		 * System.out.println("===========================");
		 * System.out.println("Product Details are:\t"); Product product =
		 * (Product)factory.getBean("product"); System.out.println(product.toString());
		 */
			
		System.out.println("============================"); 
		OrderDetails order = (OrderDetails) factory.getBean("sampleProduct");
		//System.out.println(order.toString());
		System.out.println("products ordered are:\t");
		System.out.println("============================");
		order.getProductSet().stream().forEach(product->{
			System.out.println(product.toString());
		});
		
		System.out.println("============================");
		System.out.println("Product delivered to the locations:");	 
		
		order.getAddressMap().keySet().forEach(key->{
			System.out.println(order.getAddressMap().get(key).toString());
		});
		
		
		System.out.println("============================");
		System.out.println("Locations LandMarks:");
		order.getLandMarks().stream().forEach(location->{
			System.out.println(location);
		});
		 
        

    }
}
